<?php

$price = 0;
$product = $_REQUEST['product'];
for($i = 0; $i < strlen($product); $i++) {
    $char = $product[$i];
    $asciiCode = ord($char);
    $price = ($asciiCode % 2) ? $price + $asciiCode : $price - $asciiCode;
}
echo json_encode(array('price' => $price));


?>